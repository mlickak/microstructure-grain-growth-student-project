import javax.swing.*;
import java.awt.*;

class GUI extends JFrame {
    private ButtonPanel buttonPanel;
    private ControlPanel controlPanel;
    private ThreadGrainButton threadGrainButton;
    private ThreadTime threadTime;

    private JButton run;

    GUI (int size, int time) {
        super("Rozrost ziaren");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(850, 680);
        setLocation(350, 50);

        buttonPanel = new ButtonPanel(size);
        controlPanel = new ControlPanel(size);
        Object lock = new Object();
        threadGrainButton = new ThreadGrainButton(buttonPanel, lock);
        threadTime = new ThreadTime(lock, time, threadGrainButton);


        String pauseName = "Pause";
        JButton pause = new JButton(pauseName);
        pause.addActionListener(actionEvent -> {
            if (pause.getText().equals(pauseName)) {
                threadTime.setActive(false);
                pause.setText("Re-run");
            }
            else {
                threadTime.setActive(true);
                pause.setText(pauseName);
            }
        });

        String runName = "Start";
        run = new JButton(runName);
        run.addActionListener(actionEvent -> {
            if (run.getText().equals(runName)) {
                buttonPanel.initSettingNeighbourhoodList(controlPanel.getChosenNeighbourhoodType(),
                        controlPanel.getBcComboBox());
                buttonPanel.initNucleation(
                        controlPanel.getChosenSowMethod(),
                        controlPanel.getChosenNumberOfGrains()
                );
                if (controlPanel.getChosenSowMethod().equals("Przez kliknięcie")) {
                    run.setText("Run");
                }
                else if (!buttonPanel.isActive()) {
                    controlPanel.remove(run);
                    if (threadGrainButton.isAlive()) // if restart
                        threadTime.setActive(true);
                    else {
                        threadGrainButton.start();
                        threadTime.start();
                    }
                }
//                else // unstable
//                    controlPanel.remove(run);
            } else if (!buttonPanel.isActive() & !threadGrainButton.isAlive()) { // nie uwzględnia że wątek już wystartował
                controlPanel.remove(run);
                threadGrainButton.start(); // bug
                threadTime.start();
            } else if (!buttonPanel.isActive() & threadGrainButton.isAlive() & threadTime.isAlive()) { // wątek wystartował
                threadTime.setActive(true);
            } else
                System.out.println("Nie wszystkie ziarna zostały wykorzystane");
            controlPanel.repaint();
        });

        JLabel refreshTimeText = new JLabel("Czas odświeżania");
        Integer[] refreshTime = {250, 500, 750, 1000};
        JComboBox<Integer> refreshTimeComboBox = new JComboBox<>(refreshTime);
        refreshTimeComboBox.setSelectedIndex(1);
        refreshTimeComboBox.addActionListener(actionEvent -> this.threadTime.setNewTime(refreshTime[refreshTimeComboBox.getSelectedIndex()]));

        JLabel areaSizeText = new JLabel("Zmiana wielkości planszy");
        String[] areaSize = {"10", "100", "200"};
        JComboBox<String> areaSizeComboBox = new JComboBox<>(areaSize);
        areaSizeComboBox.setSelectedIndex(1);
        areaSizeComboBox.addActionListener(actionEvent -> {
            if (Integer.parseInt(areaSize[areaSizeComboBox.getSelectedIndex()]) > 600)
                System.out.print("Not enough pixels for this window!");
            else {
                System.out.println("Size is changing, wait");
                reshapeWindow(areaSize, areaSizeComboBox);
                System.out.println("Done changing");
            }
        });

        String restartName = "Restart";
        JButton restart = new JButton(restartName);
        restart.addActionListener(actionEvent -> {
            threadTime.setActive(false);
            reshapeWindow(areaSize, areaSizeComboBox);
        });

        controlPanel.add(areaSizeText);
        controlPanel.add(areaSizeComboBox);
        controlPanel.add(refreshTimeText);
        controlPanel.add(refreshTimeComboBox);

        controlPanel.add(new JLabel(""));
        controlPanel.add(pause);
        controlPanel.add(new JLabel(""));
        controlPanel.add(restart);
        controlPanel.add(new JLabel(""));
        controlPanel.add(run);

        add(controlPanel);
        add(buttonPanel);

        setLayout(new BorderLayout());
        setVisible(true);
    }

    private void reshapeWindow(String[] areaSize, JComboBox<String> areaSizeComboBox) {
        remove(buttonPanel);
        buttonPanel = new ButtonPanel(Integer.parseInt(areaSize[areaSizeComboBox.getSelectedIndex()]));
        threadGrainButton.resetButtonPanel(buttonPanel);
        add(buttonPanel);
        if (!run.isDisplayable())
            controlPanel.add(run);
        setLayout(new BorderLayout());
        setVisible(true);
        controlPanel.repaint();
    }
}
