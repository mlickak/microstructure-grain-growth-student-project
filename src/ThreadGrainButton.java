public class ThreadGrainButton extends Thread {
    private ButtonPanel buttonPanel;
    private final Object lock;

    ThreadGrainButton(ButtonPanel buttonPanel, Object lock) {
        this.buttonPanel = buttonPanel;
        this.lock = lock;
    }

    void resetButtonPanel(ButtonPanel buttonPanel) {
        this.buttonPanel = buttonPanel;
    }

    @Override
    public void run() {
        try {
            do {
                synchronized (lock) {
                    lock.wait();
                }
                buttonPanel.step();
            } while (isAlive());
        } catch (InterruptedException e1) {
            e1.printStackTrace();
        }
    }
}