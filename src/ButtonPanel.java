import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;

public class ButtonPanel extends JPanel implements ActionListener {
    private int size;
    private Grain[][] grainArray;
    private boolean active = false;
    private Color[] primaryColors = {Color.CYAN, Color.GREEN, Color.BLUE, Color.YELLOW,
            Color.MAGENTA, Color.RED, Color.PINK, Color.BLACK, Color.ORANGE};
    private int grainsLeftToPlant;
    private Random random = new Random();

    ButtonPanel(int size) {
        this.size = size;

        setLayout(new GridLayout(size,size));
        setBounds(20,20,100,100);
        setSize(600,600);

        grainArray = new Grain[size][size];
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                grainArray[i][j] = new Grain();
                grainArray[i][j].addActionListener(this);
                add(grainArray[i][j]);
            }
        }
    }

    void step() {
        for (Grain[] grains : grainArray)
            for (Grain grain : grains)
                grain.step();
        for (Grain[] grains : grainArray)
            for (Grain grain : grains)
                grain.updateGrain();
    }

    boolean isActive() {
        return this.active;
    }

    void initSettingNeighbourhoodList(String type, String bc) {
        for (int positionX = 0; positionX < size; positionX++)
            for (int positionY = 0; positionY < size; positionY++)
                grainArray[positionX][positionY].setNeighborhoodList(grainArray, positionY, positionX, type, bc, size-1);
    }

    void initNucleation(String type, int numberOfGrains) {
        this.grainsLeftToPlant = numberOfGrains;
        switch (type) {
            case "Random":
                this.active = true;
                for (int i = 0; i < numberOfGrains; i++)
                    grainArray[random.nextInt(size)][random.nextInt(size)].doClick();
                break;
            case "Przez kliknięcie":
                this.active = true;
                break;
            case "Losowo na promieniu R":
                this.active = true;
                // weź promień R
                break;
            case "Równomiernie":
                this.active = true;
                int x = 0, y = 0;

                if (grainsLeftToPlant == 1)
                    grainArray[size / 2][size / 2].doClick();
                else if (grainsLeftToPlant == 3) {
                    x = 3+1;
                    y = 1+1;
                }
                else if (grainsLeftToPlant == 6) {
                    x = 3+1;
                    y = 2+1;
                }
                else if (grainsLeftToPlant == 9) {
                    x = 3+1;
                    y = 3+1;
                }
                else if (grainsLeftToPlant == 12) {
                    x = 4+1;
                    y = 3+1;
                }
                else if (grainsLeftToPlant == 24) {
                    x = 6+1;
                    y = 4+1;
                }

                if (x != 0) {
                    for (int j = size / y; j < size; j += size / y) {
                        for (int i = size / x; i < size; i += size / x) {
                            System.out.println("klikanko");
                            grainArray[j][i].doClick();
                        }
                    }
                }
                else
                    System.exit(1);
                break;
            default:
                System.out.println(type);
                break;
        }
    }

    @Override
    public void actionPerformed(ActionEvent event) {
        if (this.active & this.grainsLeftToPlant > 0) {
            Grain obj = (Grain) event.getSource();
            if (grainsLeftToPlant < primaryColors.length)
                obj.setColorToUpdate(primaryColors[grainsLeftToPlant]);
            else
                obj.setColorToUpdate(new Color(random.nextInt(255), random.nextInt(255), random.nextInt(255)));

            System.out.println("Colour " + obj.getColor());
            obj.updateGrain();
            grainsLeftToPlant--;
            if (grainsLeftToPlant == 0) {
                this.active = false;
            }
        }
    }
}