public class ThreadTime extends Thread {
    private final Object lock;
    private int time;
    private ThreadGrainButton tgb;
    private boolean active = true;

    ThreadTime(Object lock, int time, ThreadGrainButton tgb) {
        this.lock = lock;
        this.time = time;
        this.tgb = tgb;
    }

    void setActive(boolean active) {
        this.active = active;
    }

    void setNewTime(int time) {
        this.time = time;
    }

    @Override
    public void run() {
        try {
            do {
                Thread.sleep(time);
                if (active) {
                    synchronized (lock) {
                        lock.notifyAll();
                    }
                    if (!tgb.isAlive()) {
                        tgb.stop();
                        break;
                    }
                }
            }
            while (true);
        } catch (
                InterruptedException e) {
            e.printStackTrace();
        }
    }
}