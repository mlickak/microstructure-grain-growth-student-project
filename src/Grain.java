import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.Random;

class Grain extends JButton {
    private ArrayList <Grain> neighborhoodList = new ArrayList<>();
    private Color color;
    private boolean checkpoint = false;
    private Color updateColor = null;

    Grain(){
        setBackground(Color.WHITE);
        setBorderPainted(false);
        this.color = Color.WHITE;
    }

    Color getColor() {
        return this.color;
    }

    void step () {
        if (color!=Color.WHITE & !checkpoint)
            for (Grain g : neighborhoodList) {
                if (g.getColor() == Color.WHITE)
                    g.setColorToUpdate(this.color);
                checkpoint = true;
            }
    }

    void setColorToUpdate(Color color) {
        updateColor = color;
    }

    void updateGrain() {
        if (updateColor != null) {
            this.color = updateColor;
            updateColor = null;
            setBackground(this.color);
        }
    }

    void setNeighborhoodList(Grain[][] grainArray, int positionY, int positionX, String type, String bc, int size) {
//        this.bc = bc;
//        this.size = size-1;
        Random random = new Random();
        switch (type) {
            case "Moore":
                initSNL(grainArray, positionY, positionX, new boolean[]{true, true, true, true, true, true, true, true}, bc, size);
                break;
            case "Von Neumann":
                initSNL(grainArray, positionY, positionX, new boolean[]{false, true, false, true, false, true, false, true}, bc, size);
                break;
            case "Hexagonal Right":
                initSNL(grainArray, positionY, positionX, new boolean[]{false, true, true, true, false, true, true, true,}, bc, size); // prawy ukos
                break;
            case "Hexagonal Left":
                initSNL(grainArray,positionY,positionX,new boolean[]{true,true,false,true,true,true,false,true,}, bc, size); // lewy ukos
                break;
            case "Hexagonal Random":
                int tmp1 = random.nextInt(2);
                if (1 == tmp1)
                    initSNL(grainArray, positionY, positionX, new boolean[]{false, true, true, true, false, true, true, true,}, bc, size);
                else
                    initSNL(grainArray, positionY, positionX, new boolean[]{true, true, false, true, true, true, false, true,}, bc, size);
                break;
            case "Pentagonal Random":
                int tmp2 = random.nextInt(3);
                if (3==tmp2)
                    initSNL(grainArray, positionY, positionX, new boolean[]{true, true, true, true, false, false, false, true}, bc, size); // górne
                else if (2==tmp2)
                    initSNL(grainArray, positionY, positionX, new boolean[]{false, true, true, true, true, true, false, false}, bc, size); // prawe
                else if (1==tmp2)
                    initSNL(grainArray, positionY, positionX, new boolean[]{false, false, false, true, true, true, true, true}, bc, size); // dolne
                else
                    initSNL(grainArray, positionY, positionX, new boolean[]{true, true, false, false, false, true, true, true}, bc, size); // lewe
                break;
            default:
                System.exit(1);
                break;
        }
    }

    private void initSNL(Grain[][] grainArray, int positionY, int positionX, boolean[] type, String bc, int size) {
        // rogi zachodzą kolorem gdy kolor przechodzi na drugą stronę, dlaczego
        // każdy róg jest sąsiadem wszystkich rogów

        if (type[0] & positionY != 0 && positionX != 0)
            neighborhoodList.add(grainArray[positionX-1][positionY-1]); // górna lewa NW
//        else if (bc.equals("Periodyczne"))
//            neighborhoodList.add(grainArray[size][size]);

        if (type[1] & positionY != 0)
            neighborhoodList.add(grainArray[positionX][positionY - 1]);   // N
        else if (bc.equals("Periodyczne"))
            neighborhoodList.add(grainArray[positionX][size]);

        if (type[2] & positionX+1 != grainArray.length && positionY != 0)
            neighborhoodList.add(grainArray[positionX+1][positionY-1]); // prawa górna NE
//        else if (bc.equals("Periodyczne"))
//            neighborhoodList.add(grainArray[0][size]);

        if (type[3] & positionX + 1 != grainArray.length)
            neighborhoodList.add(grainArray[positionX + 1][positionY]);   // E prawa
        else if (bc.equals("Periodyczne"))
            neighborhoodList.add(grainArray[0][positionY]);

        if (type[4] & positionX+1 != grainArray.length && positionY+1 != grainArray.length)
            neighborhoodList.add(grainArray[positionX+1][positionY+1]); // prawa dolna SE
//        else if (bc.equals("Periodyczne"))
//            neighborhoodList.add(grainArray[0][0]);

        if (type[5] & positionY + 1 != grainArray.length)
            neighborhoodList.add(grainArray[positionX][positionY + 1]);   // S
        else if (bc.equals("Periodyczne"))
            neighborhoodList.add(grainArray[positionX][0]);

        if (type[6] & positionX != 0 && positionY+1 != grainArray[0].length)
            neighborhoodList.add(grainArray[positionX-1][positionY+1]); // lewa dolna SW
//        else if (bc.equals("Periodyczne"))
//            neighborhoodList.add(grainArray[0][size]);

        if (type[7] & positionX != 0)
            neighborhoodList.add(grainArray[positionX - 1][positionY]);   // W lewa
        else if (bc.equals("Periodyczne"))
            neighborhoodList.add(grainArray[size][positionY]);
    }
}