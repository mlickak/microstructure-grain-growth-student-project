import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Objects;

class ControlPanel extends JPanel {
    private int size;

    private JComboBox<String> neighbourhoodTypeComboBox;
    private JComboBox<String> bcComboBox;
    private JComboBox<String> sowMethodComboBox;
    private JComboBox<Integer> numberOfGrainsComboBox;

    private String[] neighbourhoodType = {"Moore", "Von Neumann", "Hexagonal Right", "Hexagonal Left", "Hexagonal Random", "Pentagonal Random"};
    private String[] sowMethod = {"Random", "Równomiernie", "Losowo na promieniu R", "Przez kliknięcie"};
    private String[] bc = {"Nieperiodyczne", "Periodyczne"};
    private Integer[] numberOfGrains = {1,5,10,20,30};

    ControlPanel(int size) {
        setBounds(640,20,170,600);
        setLayout(new GridLayout(18,1));
        this.size = size;

        JLabel neighbourhoodTypeText = new JLabel("Typ sąsiedztwa");
        neighbourhoodTypeComboBox = new JComboBox<>(neighbourhoodType);

        JLabel bcText = new JLabel("Warunek brzegowy");
        bcComboBox = new JComboBox<>(bc);

        JLabel sowMethodText = new JLabel("Zarodkowanie");
        sowMethodComboBox = new JComboBox<>(sowMethod);

        JLabel numberOfGrainsText = new JLabel("Ilość ziaren");
        numberOfGrainsComboBox = new JComboBox<>(numberOfGrains);
        numberOfGrainsComboBox.setEditable(true);
        numberOfGrainsComboBox.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                if ((int) numberOfGrainsComboBox.getSelectedItem() >= size)
                    numberOfGrainsComboBox.setSelectedIndex(2);
            }
        });

        add(neighbourhoodTypeText);
        add(neighbourhoodTypeComboBox);
        add(bcText);
        add(bcComboBox);
        add(sowMethodText);
        add(sowMethodComboBox);
        add(numberOfGrainsText);
        add(numberOfGrainsComboBox);
    }

    String getChosenNeighbourhoodType() {
        return neighbourhoodType[neighbourhoodTypeComboBox.getSelectedIndex()];
    }

    int getChosenNumberOfGrains() {
        return (int) numberOfGrainsComboBox.getSelectedItem();
    }

    String getChosenSowMethod() {
        return sowMethod[sowMethodComboBox.getSelectedIndex()];
    }

    String getBcComboBox() {
        return bc[bcComboBox.getSelectedIndex()];
    }
}